//Метод обьекта - это код обьекта в форме функции

        function createNewUser() {
            const userFirstName = prompt('what is your name?');
            const userLastName = prompt('what is your last name?');
            const newUser = {
                firstName: userFirstName,
                lastName: userLastName,
            }
            newUser.getLogin = function () {
                return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            }
            newUser.birthday = prompt('when is your birthday? dd.mm.yyyy');
            newUser.getAge = function () {
                const now = new Date();
                const year = this.birthday.slice(6);
                let month = this.birthday.slice(3,5);
                month--;
                const day = this.birthday.slice(0, 2);
                const birthResult = new Date(year, month, day);
                return Math.floor((now-birthResult)/(1000*3600*24*365));
            }
            newUser.getPassword = function () {
                return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
            }
            return newUser;

        }
        const user = createNewUser();
        console.log(user);
        console.log(user.getAge());
        console.log(user.getPassword());